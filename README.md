# lit-hook

[![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo)
[![Coverage Status](https://coveralls.io/repos/bitbucket/robmarr/lit-hook/badge.svg?branch=master)](https://coveralls.io/bitbucket/robmarr/lit-hook?branch=master)
[![build](https://img.shields.io/bitbucket/pipelines/robmarr/lit-hook.svg)](https://bitbucket.org/robmarr/lit-hook/addon/pipelines/home)
[![version](https://img.shields.io/npm/v/lit-hook/latest.svg)](https://www.npmjs.com/package/lit-hook)
[![chat](https://img.shields.io/discord/536945285989924864.svg)](https://discord.gg/UgcRF6t)

A require hook that compiles lit-html files

## Installation

`npm i lit-hook`

## Usage

This hook takes a lit-html file and returns an async function that renders the template to an HTML string,

Most lit html directives are supported via [@popeindustries/lit-html-server](https://github.com/popeindustries/lit-html-server).

```javascript

const addLitHook = require('lit-hook')
const revert = addLitHook()

const template = require('./fixtures/basic.lit')
// <div>${locals.basic}</div>
await template({ basic: 'hello' })
// <div>hello</div>

revert() // stop the hook from running
```

### Options

```javascript
addLitHook({exts: ['.lit'], matcher: file => file.startsWith('z'), portable: true, wrapper: code => `(context, locals) => context.renderToStream(context.html)`})
```

#### `exts`

The file extensions targeted by the hook default: `['.lit']` so to add html file compilation use `['.lit', '.html']`

#### `matcher function(filename)`  

With this method you can inspect the filename to determine if it should be hooked or not.

Just return a truthy/falsy. Files in node_modules are ignored, unless otherwise specified in options ([read more on pirates](https://www.npmjs.com/package/pirates)).

The default matcher returns true for all files.

#### `portable boolean`

Changes the hook to produce portable templates. Portable templates require you to supply the rendering context (render, html, directives, etc).

#### `wrapper function(code)`

Allows the customization of the code wrapper used by the hook.

For example with the code:

 > `'hello world'`

and the custom wrapper:

> `(code) => ('module.exports = () => (code)`

files will look like this when required:

> `module.exports = () => ('hello world')`

## License

Unless stated otherwise all works are:

- Copyright &copy; Robin Marr

and licensed under:

- [MIT License](http://spdx.org/licenses/MIT.html)

## Thanks

Thanks to [Alexander Pope](https://github.com/popeindustries) for [lit-html-server](https://github.com/popeindustries/lit-html-server) and [ariporad](https://github.com/ariporad/) for [pirates](https://github.com/ariporad/pirates) they made this a breeze, also thanks to [Justin Fagnani](https://github.com/justinfagnani) and the [team](https://github.com/Polymer/lit-html/graphs/contributors) behind the [**lit-html**](https://github.com/Polymer/lit-html/) project!
