import test from 'ava'
import {html} from '@popeindustries/lit-html-server'
import rendering from './helpers/render'

const empty = {name: 'empty', args: {}}

rendering.title = (_, {template, locals}) => `${template} directive ${locals ? `with ${locals.name ? locals.name : locals} locals` : ''}`.trim()

test(rendering, {template: 'cache', locals: {name: 'detail view', args: {showDetails: true, detailView: () => html`<h1>Details</h1>`}}})
test(rendering, {template: 'cache', locals: {name: 'summary view', args: {showDetails: false, summaryView: () => html`<h1>Summary</h1>`}}})
test(rendering, {template: 'classMap', locals: empty})
test(rendering, {template: 'guard'})
test(rendering, {template: 'ifDefined', locals: 'defined'})
test(rendering, {template: 'ifDefined', locals: 'undefined'})
test(rendering, {template: 'repeat'})
test(rendering, {template: 'styleMap', locals: empty})
test(rendering, {template: 'unsafeHTML-with'})
test(rendering, {template: 'unsafeHTML-without'})
test(rendering, {template: 'until'})
test(rendering, {template: 'until', locals: {name: 'unresolved-promise', args: {content: new Promise((resolve, reject) => ([resolve, reject]))}}})
test(rendering, {template: 'until', locals: empty})
