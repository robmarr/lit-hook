const {readFile} = require('fs').promises
const {html, renderToString: render} = require('@popeindustries/lit-html-server')

const {hook} = require('../pure')

/* eslint-disable no-eval */
// we need to eval the code produced to see if it runs
const rendering = async (t, input) => {
  const {template: templateName, locals: localsName, portable, wrapper} = input
  const code = await readFile(require.resolve(`../fixtures/${templateName}.lit`), {encoding: 'utf8'})
  const locals = localsName && localsName.args ? localsName.args : require(`../fixtures/${templateName}${localsName ? `-${localsName}` : ''}.locals.json`)

  const compiled = hook(wrapper)(code)
  const template = eval(compiled)
  const rendered = portable ? await template({html, render}, locals) : await template(locals)
  t.snapshot(code)
  t.snapshot(compiled)
  t.snapshot(rendered)
}
/* eslint-enable no-eval */

rendering.title = (_, {template, locals}) => `render ${template} template ${locals ? `with ${locals.name ? locals.name : locals} locals` : ''}`.trim()
module.exports = rendering
