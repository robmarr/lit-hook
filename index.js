import {addHook} from 'pirates'
import {addLitHook, cacheClean} from './pure.js'

export default addLitHook(addHook, cacheClean)
