import {addHook} from 'pirates'
import {addLitHook, cacheClean} from './pure'

export default addLitHook(addHook, cacheClean)
