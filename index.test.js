import test from 'ava'
import rendering from './helpers/render.js'

import addLitHook from './index.js'

const file = require.resolve('./fixtures/basic.lit')
rendering.title = () => 'on require .lit files are compiled'

test(rendering, {template: 'basic', locals: {args: {basic: 'basic'}}})

test('revoking the hook and attempting to compile a .lit file will result in an error', t => {
  delete require.cache[file]
  const revoke = addLitHook({})
  revoke()
  const thrower = () => require(file)
  t.throws(thrower)
})
