import _debug from 'debug'

const debug = _debug('lit-hook')

const defaultWrapper = code => {
  return 'const {html, renderToString} = require(\'@popeindustries/lit-html-server\')\n' +
  'const {cache} = require(\'@popeindustries/lit-html-server/directives/cache\')\n' +
  'const {classMap} = require(\'@popeindustries/lit-html-server/directives/class-map\')\n' +
  'const {guard} = require(\'@popeindustries/lit-html-server/directives/guard\')\n' +
  'const {ifDefined} = require(\'@popeindustries/lit-html-server/directives/if-defined\')\n' +
  'const {repeat} = require(\'@popeindustries/lit-html-server/directives/repeat\')\n' +
  'const {styleMap} = require(\'@popeindustries/lit-html-server/directives/style-map\')\n' +
  'const {unsafeHTML} = require(\'@popeindustries/lit-html-server/directives/unsafe-html\')\n' +
  'const {until} = require(\'@popeindustries/lit-html-server/directives/until\')\n' +
  'module.exports = (locals) => renderToString(html`' + code.trim() + '`\n)'
}

export const portableWrapper = code => {
  return 'module.exports = (context, locals) => {\n' +
  '  const {cache, classMap, guard, ifDefined, repeat, styleMap, unsafeHTML, until} = context.directives ? context.directives : context\n' +
  '  return context.render(context.html`' + code.trim() + '`\n)' +
  '}\n'
}

export const hook = wrapper => (code, filename) => {
  debug(`compiling ${filename}`)
  const wrap = wrapper || defaultWrapper
  return wrap(code)
}

export const baseMatcher = () => true
export const precompiledMatcher = () => true
export const cacheClean = file => (
  require.cache && require.cache[file] && delete require.cache[file]
)

export const addLitHook = (addHook, cacheClean) => options => {
  const files = []
  const cacheFile = file => file ? files.push(file) : null
  // Option defaults
  const {
    clearCache = false,
    exts = ['.lit'],
    file = null,
    matcher = baseMatcher,
    portable = false,
    wrapper: customWrapper = false
  } = options || {}
  // If requested remove the supplied file from require cache
  if (clearCache) {
    debug(`clearing cache of ${file}`)
    cacheFile(file)
  }

  debug('adding compilation hook')
  const wrapper = portable ? portableWrapper : customWrapper
  // Create the hook and return the revert function
  const revert = addHook(hook(wrapper), {exts, matcher})
  return () => {
    // If requested remove the supplied file from require cache
    if (clearCache) {
      files.forEach(file => {
        debug(`clearing cache of ${file}`)
        cacheClean(file)
      })
    }

    debug('reverting compilation hook')
    revert()
  }
}
