import test from 'ava'
import {stub, match} from 'sinon'
import rendering from './helpers/render.js'

import {addLitHook, baseMatcher, cacheClean, portableWrapper} from './pure.js'

// Setup dependency stubs, spies and fixtures
const revert = stub()
const mocks = {
  revert, // The function returned by addHook
  addHook: stub().returns(revert), // Stub our main dependency so we can check how it's used
  file: require.resolve('./fixtures/basic') // A file path for testing caching.
}

// "Reset" stubs after each test
test.afterEach(() => {
  const {revert, addHook} = mocks
  revert.reset()
  addHook.reset()
})

test('hook setup with defaults', t => {
  t.plan(1)
  const {addHook} = mocks
  addLitHook(addHook)()
  t.true(addHook.calledWithExactly(match.func, {exts: ['.lit'], matcher: match.func})) // Check it was called with default options
})

test('revert still works', t => {
  t.plan(1)
  const {addHook, revert} = mocks
  const revoke = addLitHook(addHook)()
  revoke()
  t.true(revert.calledOnce) // Make sure our function returns the same function returned by addHook
})

test('override default extensions (.lit)', t => {
  t.plan(1)
  const {addHook} = mocks
  addLitHook(addHook)({exts: ['.html']})
  t.true(addHook.calledWith(match.func, {exts: ['.html'], matcher: match.func})) // The hook was setup with our custom extensions
})

test('override default file matcher (noop)', t => {
  t.plan(1)
  const {addHook} = mocks
  const matcher = 'matcher'
  addLitHook(addHook)({matcher})
  t.true(addHook.calledWith(match.func, {exts: match.any, matcher})) // The hook was setup with our custom matcher
})

test('cacheClean clears the require cache', t => {
  t.plan(2)
  const {file} = mocks
  require(file)

  t.truthy(require.cache[file]) // Check it's in there
  cacheClean(file)
  t.falsy(require.cache[file]) // Check it's  gone
})

test('cache clearing runs when the hook is reverted', t => {
  t.plan(1)
  const {file, addHook} = mocks
  const cacheClean = stub()
  const revert = addLitHook(addHook, cacheClean)({file, clearCache: true})
  require(file)
  revert()
  t.truthy(cacheClean.calledOnce) // Our clear function was called
})

test('clearing defaults to off', t => {
  t.plan(1)
  const {file, addHook} = mocks
  const cacheClean = stub()
  addLitHook(addHook, cacheClean)({file})
  t.falsy(cacheClean.called) // Our clear function was not called
})

test('set cache clearing to off', t => {
  t.plan(1)
  const {file, addHook} = mocks
  const cacheClean = stub()
  addLitHook(addHook, cacheClean)({file, clearCache: false})
  t.falsy(cacheClean.called) // Our clear function was not called
})

test('cache clearing, no file = noop', t => {
  t.plan(1)
  const {addHook} = mocks
  const cacheClean = stub()
  addLitHook(addHook, cacheClean)({clearCache: false})
  t.falsy(cacheClean.called) // Our clear function was not called
})

test('baseMatcher just returns true all the time', t => {
  t.plan(4)
  t.true(baseMatcher('some/file/path'))
  t.true(baseMatcher(false))
  t.true(baseMatcher({}))
  t.true(baseMatcher([]))
})

rendering.title = () => 'compiled components can be rendered'
test(rendering, {template: 'basic', locals: {args: {basic: 'Ya Basic!'}}})

rendering.title = () => 'portable components can be rendered'
test(rendering, {template: 'basic', portable: true, wrapper: portableWrapper, locals: {args: {basic: 'Ya Basic!'}}})

rendering.title = () => 'custom wrappers can be used'
const customWrapper = () => '(locals) => { return "Ya Bespoke!"}'
test(rendering, {template: 'basic', wrapper: customWrapper, locals: {args: {basic: 'Ya Basic!'}}})
