# Snapshot report for `pure.test.js`

The actual snapshot is saved in `pure.test.js.snap`.

Generated by [AVA](https://ava.li).

## compiled components can be rendered

> Snapshot 1

    `<div>${locals.basic}</div>␊
    `

> Snapshot 2

    `const {html, renderToString} = require('@popeindustries/lit-html-server')␊
    const {cache} = require('@popeindustries/lit-html-server/directives/cache')␊
    const {classMap} = require('@popeindustries/lit-html-server/directives/class-map')␊
    const {guard} = require('@popeindustries/lit-html-server/directives/guard')␊
    const {ifDefined} = require('@popeindustries/lit-html-server/directives/if-defined')␊
    const {repeat} = require('@popeindustries/lit-html-server/directives/repeat')␊
    const {styleMap} = require('@popeindustries/lit-html-server/directives/style-map')␊
    const {unsafeHTML} = require('@popeindustries/lit-html-server/directives/unsafe-html')␊
    const {until} = require('@popeindustries/lit-html-server/directives/until')␊
    module.exports = (locals) => renderToString(html`<div>${locals.basic}</div>`␊
    )`

> Snapshot 3

    '<div>Ya Basic!</div>'

## custom wrappers can be used

> Snapshot 1

    `<div>${locals.basic}</div>␊
    `

> Snapshot 2

    '(locals) => { return "Ya Bespoke!"}'

> Snapshot 3

    'Ya Bespoke!'

## portable components can be rendered

> Snapshot 1

    `<div>${locals.basic}</div>␊
    `

> Snapshot 2

    `module.exports = (context, locals) => {␊
      const {cache, classMap, guard, ifDefined, repeat, styleMap, unsafeHTML, until} = context.directives ? context.directives : context␊
      return context.render(context.html`<div>${locals.basic}</div>`␊
    )}␊
    `

> Snapshot 3

    '<div>Ya Basic!</div>'
