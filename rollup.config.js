
import commonjs from 'rollup-plugin-commonjs'
import {name, dependencies} from './package.json'

const config = {
  input: 'index.js',
  output: [{
    name,
    file: 'dist/index.js',
    format: 'cjs',
    exports: 'default'
  }, {
    name,
    file: 'dist/index.mjs',
    format: 'esm'
  }],
  external: Object.keys(dependencies),
  plugins: [commonjs()]
}

export default config
