
import commonjs from 'rollup-plugin-commonjs'
// It breaks if we don't supply the json extension
// eslint-disable-next-line import/extensions
import {name, dependencies} from './package.json'

export default {
  input: 'index.js',
  output: [{
    name,
    file: 'dist/index.js',
    format: 'cjs'
  }, {
    name,
    file: 'dist/index.mjs',
    format: 'esm'
  }],
  external: Object.keys(dependencies),
  plugins: [commonjs()]
}
